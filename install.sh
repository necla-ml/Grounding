#!/bin/bash

set -e
#set -x

# Assume miniconda and pip3 are installed for managing packages on ubuntu 16.04 LTS.
# Python 3.7+ and PyTorch 1.1.* are required.

# Create and import the SINet environment for Python 3.7
# Make sure to append the conda env activation to ~/.bashrc

ENV=vet37
if [ "$CONDA_DEFAULT_ENV" = "$ENV" ]; then
    :
else
    eval "$(conda shell.bash hook)"
    if conda activate $ENV; then
        echo Activated $CONDA_DEFAULT_ENV
    elif conda env create -f env.yml; then
    #elif conda create -n $ENV; then
		#conda update -n base -c defaults conda
        echo $SHELL: Created conda env $ENV
        conda activate $ENV || (echo Failed to activate created conda env $ENV; exit 1)
		conda clean -ya
        echo $SHELL: Activated $CONDA_DEFAULT_ENV
    else
        echo Failed to create conda env $ENV
        exit 1
    fi
fi

# Pillow-SIMD to replace Pillow
pip uninstall --yes pillow
CC="cc -mavx2" pip install -U --force-reinstall pillow-simd