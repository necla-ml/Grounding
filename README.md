# Contextual Grounding

Reference implementation of [Contextual Grounding of Natural Language Entities in Images](http://bit.ly/380WWMr) accepted to ViGIL workshop, NeurIPS 2019.

```bibtex
@misc{lai2019contextual,
    title={Contextual Grounding of Natural Language Entities in Images},
    author={Farley Lai and Ning Xie and Derek Doran and Asim Kadav},
    year={2019},
    eprint={1911.02133},
    archivePrefix={arXiv},
    primaryClass={cs.CV}
}
```

## Reproducing the Results

1. Clone the repo and install the conda environment assuming the base conda is available

    ```sh
    GIT_LFS_SKIP_SMUDGE=1 git clone git@gitlab.com:necla-ml/Grounding.git
    cd Grounding
    ./install.sh
    conda activate vet37
    ```

    Note the pre-trained model checkpoint is available as a Git LFS object.
    Remove `GIT_LFS_SKIP_SMUDGE=1 ` to clone the repository will download it by default.

2. Build and install the ML supporting package

    ```sh
    make pull
    cd submodules/ML
    make build install
    ```

3. Download the Flickr30K dataset and pre-extracted RoI features

    ```sh
    cd data
    ./download
    ```

4. Download the pretrained model from LFS Storage

    ```sh
    git lfs pull
    ```

5. Evaluation to reproduce the results

    Run the following command to evaluate the performance over the test split.
    The feature extraction would be triggered for the first time if necessary.

    ```sh
    ./main.py test @configs/cfg-s1204-L1-H2-dp0.4-b256-lr0.00005-wp0.1-abs.args --resume export/bert-adam-s1204-L1-H2-dp0.4-b256-lr0.00005-wp0.1-abs/grounding_model_5_recall\=0.7136248.pth --split test
    ```

    The results will be reported as overall and per entity type recalls nearly at the end:

    ```sh
    Test loss=2.9655, recalls=(71.36, 84.76, 86.49, 87.45), per type=(81.95, 76.5, 46.27, 82.05, 79.0, 35.8, 70.23, 53.53), time=15.020s
    ```

    If there is any inconsistence, make sure the generated `test_imgid2idx.pkl` and `test_entities.pt` in `data/Flickr30K` have the same md5sum values:
    - 850ccced3a7cf7db7fc674290d06793d  `test_imgid2idx.pkl`
    - a2aa6ab8b40fae5c0bf37ae11a870c63  `test_entities.pt`

## Model Training from Scratch

Start Visdom server to visualize the progress.  
In a separate terminal, run the following command:

```sh
python -m visdom.server
```

The progress can be visualized at http://localhost:8097.

Next, assuming all the required data is downloaded, run the following command to train the model:

```sh
./main.py train @configs/cfg-s1204-L1-H2-dp0.4-b256-lr0.00005-wp0.1-abs.args --split train,test --grad-acc-steps 2
```

By default, all the GPUs will be used and the feature extraction on the fly is necessary for the first time.  
The experimental results are based on two RTX Titans with sufficient memory.  
The default CUDA device may consume more than `14GB` during training.  
If there is any inconsistence, make sure the generated `train_imgid2idx.pkl` and `train_entities.pt` in `data/Flickr30K` have the same md5sum values:

- c11eeb0810423eb0304b0780dbba7179  `train_imgid2idx.pkl`
- 84c855844ae70af05715df7a824e92c2  `train_entities.pt`

