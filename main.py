#!/usr/bin/env python

import os
import sys
from pathlib import Path

import numpy as np
import torch as th

from ml.utils import Config, strf
from ml import argparse
import ml


def parse_args(argv=None):
    parser = argparse.ArgumentParser(
        description="Grounding over Flickr30K Entities", fromfile_prefix_chars="@"
    )
    parser.add_argument(
        "cmd",
        default="test",
        choices=["train", "test"],
        help="Command action to take",
    )

    # Dataset
    parser.add_argument("--data", default="data", help="Path to dataset")
    parser.add_argument("--dataset", default="Flickr30K", help="Dataset to train and test")
    parser.add_argument(
        "--split", default="train,val", help="One or more dataset splits to select"
    )
    parser.add_argument(
        "--index", default=0, type=int, help="Index to an example in the split"
    )
    parser.add_argument("--imgid", default=None, type=int, help="Image id in Flickr30K")
    parser.add_argument(
        "--tok",
        default="wordpiece",
        choices=["split", "wordpiece"],
        help="Plain string split or wordpiece tokenization.",
    )

    # System parameters
    parser.add_argument(
        "-j", "--workers", type=int, default=0, help="number of threads to allocate"
    )

    # Grounding task specific
    parser.add_argument("--arch", default="bert", choices=["bert"])
    parser.add_argument(
        "--max-tokens",
        type=int,
        default=88,
        help="max number of caption tokens to allocate",
    )
    parser.add_argument(
        "--max-entities",
        type=int,
        default=16,
        help="max number of caption entities to allocate",
    )
    parser.add_argument(
        "--max-rois", type=int, default=100, help="max number of RoIs to allocate"
    )

    # Dual BERT modalities for Contextual Grounding
    parser.add_argument(
        "--bert-img-hidden-size", type=int, default=2048, help="Image embedding dimension"
    )
    parser.add_argument(
        "--bert-img-intermediate-size", type=int, default=3072, help="Final image encoding dense dimension"
    )
    parser.add_argument(
        "--bert-img-layers",
        type=int,
        default=3,
        help="Number of image transformer layers",
    )
    parser.add_argument(
        "--bert-img-heads", type=int, default=2, help="Number of image attention heads"
    )
    parser.add_argument(
        "--bert-img-hidden-dp", type=float, default=0.5, help="BERT hidden layer dropout prob"
    )
    parser.add_argument(
        "--bert-img-attention-dp", type=float, default=0.5, help="BERT attention dropout"
    )
    parser.add_argument(
        "--bert-img-spatial", type=str, default=None, choices=[None, 'abs', 'rel'], help="BERT spatial encoding for images"
    )

    # Training and Testing
    parser.add_argument(
        "--fp16",
        action="store_true",
        help="Whether to enable 16-bit float precision instead of 32-bit.",
    )
    parser.add_argument(
        "--epochs", type=int, default=9, help="Number of training epochs"
    )
    parser.add_argument(
        "--bs", type=str, default="256", help="batch size for each split"
    )
    parser.add_argument(
        "--grad-acc-steps",
        type=int,
        default=2,
        help="Number of steps to accumulate gradients before an update pass to save memdory.",
    )
    parser.add_argument("--lr", type=float, default=5e-5, help="learning rate")
    parser.add_argument(
        "--warmup",
        default=0.1,
        type=float,
        help="Proportion of training to perform linear learning rate warmup e.g., 0.1 = 10%% of training.",
    )

    # Optimizer specific
    parser.add_argument(
        "--optim",
        type=str,
        default="adam",
        choices=["adam", "adamax"],
        help="Optimizer to train",
    )
    parser.add_argument("--momentum", type=float, default=0.9, help="SGD momentum")
    parser.add_argument("--adam-wd", type=float, default=1e-2, help="weight decay")
    parser.add_argument("--adam-beta1", type=float, default=0.9, help="Adam beta1")
    parser.add_argument(
        "--adam-beta2", type=float, default=0.999, help="Adam beta2"
    )
    parser.add_argument(
        "--adam-eps", type=float, default=1e-6, help="Adam epsilon"
    )
    parser.add_argument(
        "--max-grad-norm", type=float, default=0.25, help="Max grad norm to clip"
    )

    # Misc
    parser.add_argument(
        "--log-interval",
        type=int,
        default=8,
        help="frequency in batch iterations to show training progress",
    )
    parser.add_argument(
        "--nsaved", type=int, default=3, help="Number of top performant models to save"
    )
    parser.add_argument(
        "--resume", type=str, default=None, help="path to saved model to resume"
    )

    if argv is None:
        args = parser.parse_args()
    else:
        argv = type(argv) is str and argv.split() or argv
        args = parser.parse_args(argv)

    return Config(args=args)


def setup(cfg):
    """
    Essential setup for training:
        root: project directory
        data: path to dataset
        shared: path to NFS shared data
        model: formatted string combining architecture and configurations
        export: path to generate output
        save: path to save trained model, results, and logs
        split: one or more dataset splits to use
        bs: batch size for each split


    Args:
        cfg: configuration to set up
    """

    cfg.root = Path(__file__).resolve().parent
    cfg.data = cfg.root / cfg.data
    cfg.model = f"{cfg.arch}-{cfg.optim}-s{cfg.seed}-"\
                f"L{cfg.bert_img_layers}-H{cfg.bert_img_heads}-dp{cfg.bert_img_hidden_dp}-"\
                f"b{cfg.bs}-lr{strf(cfg.lr)}-wp{cfg.warmup}"\
                f"{'' if cfg.bert_img_spatial is None else f'-{cfg.bert_img_spatial}'}"
    cfg.export = cfg.root / "export"
    cfg.save = cfg.export / cfg.model
    cfg.save.mkdir(parents=True, exist_ok=True)

    SPLITS = ["train", "val", "test"]
    cfg.split = cfg.split.split(",")
    assert all(s in SPLITS for s in cfg.split)

    cfg.bs = [int(bs) for bs in cfg.bs.split(",")]
    if cfg.cmd == "train":
        if len(cfg.split) == 1:
            cfg.split *= 2
        if len(cfg.bs) == 1:
            cfg.bs *= 2
    else:
        cfg.split = cfg.split[0]
        cfg.bs = cfg.bs[0]

    return cfg


def main(cfg):
    from ml.tasks.grounding import runner
    runner.run(cfg)


if __name__ == "__main__":
    cfg = parse_args()
    setup(cfg)
    ml.run(main, cfg)
